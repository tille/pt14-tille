/** @file
    @brief Header file defining the Function class interface.
*/

#ifndef FUNCTION_GUARD_
#define FUNCTION_GUARD_


/** 
    @brief Abstract base class providing an interface to objects defining the function call operator (function objects).
*/
struct Function {
    typedef double result_type; /**< Type specifying the numerical type used for the range of the function object.*/	
    typedef double argument_type; /**< Type specifying the numerical type used for the domain of the function object.*/ 
    virtual result_type operator()(argument_type x) const=0; /**< Purely virtual function call operator */
    virtual ~Function() {} /**< Virtual destructor. */
};

#endif 
