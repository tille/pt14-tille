#include <ctime>
#include <sys/time.h>
#include "random.hpp"

#define GENERATOR_MODULO 4294967296l // 2^32
#define GENERATOR_A 1664525l
#define GENERATOR_C 1013904223l

Generator::Generator() :
        _m(GENERATOR_MODULO), 
        _a(GENERATOR_A),
        _c(GENERATOR_C)

    {
        // get time
        struct timeval t;
        gettimeofday(&t, NULL);

        _x_n = t.tv_sec;
    };

Generator::Generator(const int initial_seed): 
        _x_n(initial_seed), 
        _m(GENERATOR_MODULO), 
        _a(GENERATOR_A), 
        _c(GENERATOR_C) {};

Generator::Generator(const int initial_seed, const long m, const int a, const int c) : 
        _x_n(initial_seed), 
        _m(m),
        _a(a),
        _c(c) {};

long Generator::generate () {
    return _x_n = (_a * _x_n + _c) % _m;
};

long Generator::max () {
    return _m;
};