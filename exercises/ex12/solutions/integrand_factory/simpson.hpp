/** @file
    @brief Header file declaring the simpson integration routine.
*/

/** @fn Function::result_type integrate(const Function::argument_type a, const Function::argument_type b, const unsigned bins, const Function& f)
    @param [in] a Beginning (End) of integration interval.
    @param [in] b End (Beginning) of integration interval.
    @param [in] bins Number of bins to be used for the discretization of the integration interval.
    @param [in] f Object of class type derived from Function class used for the integration.

    @pre 

    - The domain of the function @p f shall contain the interval [min(@p a,@p b),max(@p a,@p b)].
    - The boundary values @p a and @p b shall be convertible to Function::argument_type type.
    - The parameter @p bins shall be convertible to unsigned int type.
    - The parameter @p bins shall be strictly non-negative ( bins > 0 ). 
 
    @post 
   
    - The value returned will approximate the integral of
      the function f(x) over the interval [min(a,b),max(a,b)]
      by the use of the Simpson rule with 'bins' equally sized
      intervals.
 
    @throw std::logic_error
    will be thrown in case the number of bins provided is not strictly positive. 
 
   The interval given by the boundaries 'a' and 'b' will be divided into 'bins' equally sized bins. The function f will be aproximated by a parabola using the function values at the bin-boundaries and bin-midpoint. the integral will be approximated by the sum of the integrals over each bin of the corresponding interpolating parabola. This corresponds to the simpson integration method explained in http://en.wikipedia.org/wiki/Simpson's_rule. The number of function calls is 2*bins+1. 
*/

#ifndef SIMPSON_GUARD_
#define SIMPSON_GUARD_

#include "function.hpp"

Function::result_type integrate(const Function::argument_type a, const Function::argument_type b, const unsigned bins, const Function& f);

#endif /* SIMPSON_GUARD_ */
