/** @file
    @brief Header file declaring the integrandFactory function and supported function objects.
*/

/** @struct sin_lambda
    @brief function object implementing the operation sin(lambda*x).
*/

/** @struct cos_lambda
    @brief function object implementing the operation cos(lambda*x).
*/

/** @struct exp_lambda
    @brief function object implementing the operation exp(lambda*x).
*/

/** @fn Function* integrandFactory(std::string function_name , Function::argument_type lambda ) 
    @brief function returning a pointer to an object of type Function. This function can be used to select which function object will be constructed out of the available options: sin_lambda, cos_lambda, exp_lambda..
    @param [in] function_name Specifies the type of function object to generate.
    @param [in] lambda Scaling parameter used when invoking the function call operator of the generated function object.
	@throw std::range_error is thrown in case @p function_name does not match any implemented function.
	@remark The pointer returned to the user will correspond to an object allocated dynamically by an internal call to @p new. The user should ensure the proper disposal of any object created using the @p integrandFactory with a corresponding @p delete call. 
*/

#ifndef INTEGRAND_GUARD_
#define INTEGRAND_GUARD_

#include <string>
#include <cmath>
#include "function.hpp"

struct sin_lambda : public Function
{
    /// @param[in] lambda Scaling parameter.
    sin_lambda( argument_type lambda = 1. ): lambda_(lambda){}
    
    /// @return sin(lambda*x).
    result_type operator()( argument_type x ) const { return std::sin(lambda_*x);  }

private:
    argument_type lambda_;
};

struct cos_lambda : public Function
{
    /// @param[in] lambda Scaling parameter.    
    cos_lambda( argument_type lambda = 1. ): lambda_(lambda){}
    
    /// @return cos(lambda*x).
    result_type operator()( argument_type x ) const { return std::cos(lambda_*x);  }

private:
    argument_type lambda_;
};

struct exp_lambda : public Function
{
    /// @param[in] lambda Scaling parameter.
    exp_lambda( argument_type lambda = 1. ): lambda_(lambda){}
    
    /// @return  exp(lambda*x).
    result_type operator()( argument_type x ) const { return std::exp(lambda_*x); }

private:
    argument_type lambda_;
};

Function* integrandFactory(std::string function_name , Function::argument_type lambda );

#endif /* INTEGRAND_GUARD_ */
