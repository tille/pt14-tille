// copyright by me
#ifndef ARRAYBOOL_HPP
#define ARRAYBOOL_HPP

#include <utility> // for std::swap
#include <cassert>
#include "array.hpp"


//
// b = false;
//

template <class I>
class BitReference {
public:
  typedef I integer_type;
  BitReference(integer_type& x, int b)
   : word_(x), bit_(b) {}

  operator bool() const { return (word_ >> bit_) & 1; }

  BitReference const& operator=(bool x)
  {
    if (x) 
      word_ = word_ | (1<<bit_);
    else
      word_ = word_ & ~(1<<bit_);
    return *this;
  }



private:
  integer_type& word_;
  int bit_;
};


template <> 
class Array<bool>
{
  typedef unsigned long long integer_type;
public: 
  typedef std::size_t size_type;
  typedef bool value_type;
  typedef BitReference<integer_type> reference;
  typedef BitReference<integer_type> const const_reference;
  
  Array(); // empty array
  Array(size_type); // Array of given size
  Array(const Array<bool>&); // copy of an Array
  ~Array();
  
  Array<bool>& operator= (Array<bool>); // assign another array
  
  size_type size() const;  // the size of the array
  void resize(size_type); // change the size of the array  
  
  reference operator[](size_type index); // a[i]=false;
  value_type operator[](size_type index) const; // bool x=a[i];
  
  void swap(Array<bool>& x);

  // might want to add: void sort();
    
private:
  static const int bits_per_word = std::numeric_limits<integer_type>::digits;
  
  size_type number_of_words(size_type s) 
  {
    return (s+bits_per_word-1)/bits_per_word;
  }

  size_type sz_; // size
  integer_type* v_;   // pointer to the actual array
};


template <>
Array<bool>::Array() 
 : sz_(0)
 , v_(0)
{
}


template <> 
Array<bool>::Array(size_type s)
 : sz_(s)
 , v_(new integer_type[number_of_words(sz_)])
{
}


template <> 
Array<bool>::Array(const Array<bool>& rhs)
 : sz_(rhs.sz_)
 , v_(new value_type[sz_])
{
  for (size_type i = 0;i<number_of_words(sz_);i++)
    v_[i] = rhs.v_[i];
}


template <> 
Array<bool>::~Array()
{
  delete[] v_;
}


template <> 
void Array<bool>::swap(Array<bool>& rhs)
{
  std::swap(sz_,rhs.sz_);
  std::swap(v_,rhs.v_);
}


// and the best version: pass by value so that a copy is already created
template <> 
Array<bool>& Array<bool>::operator = (Array<bool> rhs)
{
  swap(rhs);
  return *this;
}


template <> 
Array<bool>::size_type Array<bool>::size() const  // the size of the array
{
  return sz_;
}



template <> 
void Array<bool>::resize(size_type s)
{
  Array<bool> w(s);
  for(int i = 0; i<number_of_words(sz_) && i<number_of_words(s); i++)
    w.v_[i] = v_[i];
  swap(w);        
} 


template <> 
Array<bool>::reference Array<bool>::operator[](size_type index) 
{
  assert (index>=0 && index < size());
  return reference(????);
}


template <>
Array<bool>::value_type Array<bool>::operator[](size_type index) const
{
  assert (index>=0 && index < size());
  size_type word = index / bits_per_word;
  size_type bit  = index % bits_per_word;
  return (v_[word] >> bit) & 1;
}

#endif
