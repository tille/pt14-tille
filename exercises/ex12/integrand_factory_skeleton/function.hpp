#ifndef FUNCTION_GUARD_
#define FUNCTION_GUARD_

struct Function {
    typedef double result_type; 
    typedef double argument_type;
    virtual result_type operator()(argument_type x) const=0;
    virtual ~Function() {} 
};

#endif 
