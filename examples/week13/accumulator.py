# Programming Techniques for Scientific Simulations, HS 2014
# Week 13

from math import sqrt

class Accumulator:
  """ Accumulator class provides simple statistical evaluation suitable for uncorrelated statistical data """
  
  def __init__(self):
    self.reset()
  
  def reset(self):
    self._sum0=0
    self._sum1=0
    self._sum2=0
  
  def add(self,value):
    self._sum0+=1
    self._sum1+=value
    self._sum2+=value*value
  
  def __lshift__(self,value):
    self.add(value)

  def number(self):
    return self._sum0
  
  def mean(self):
    if self._sum0==0: raise ValueError("Accumulator.mean(): no measurement available")
    return self._sum1/float(self._sum0)
  
  def variance(self):
    if self._sum0<2: raise ValueError("Accumulator.variance(): not enough measurements available.")
    return (self._sum2-self._sum1*self._sum1/float(self._sum0))/float(self._sum0)
  
  def stddev(self):
    return sqrt( self.variance() / (1. - 1./self._sum0) )
  
  def meanerr(self):
    return sqrt( self.variance() / (self._sum0-1.) )
    
  def __str__(self):
    return "mean: "+str(self.mean())+' +/- '+str(self.meanerr())+", stddev: "+str(self.stddev())+", count: "+str(self._sum0)
  
  def __repr__(self):
    return "Accumulator("+str(self._sum0)+","+str(self._sum1)+","+str(self._sum2)+")"
