#include <vector>
#include <list>
#include <iostream>
#include <random>
#include <set>

#include "benchmark_suites.cpp"

using std::cout;
using std::endl;

// pre: buffer is a valid pointer to an array of size buffer_size

//if you just want to fill buffer, return void
template <class T>
void random_number_buffer(T* buffer, const std::size_t buffer_size, T lower_boundary, T upper_boundary) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
 	std::uniform_int_distribution<T> distribution(lower_boundary,upper_boundary);

	for (std::size_t i=0; i<buffer_size; i++) {
		buffer[i] = distribution(generator);
	}
}

template <class T> using time_point = std::chrono::time_point<T>;

// alias for value Type
template <class T> using V = typename T::value_t;

//for debug secitons use the preprocessor, not a template argument!!:
/*
#ifndef NDEBUG
....
#endif
*/

// pre: T is a benchmark suite
template <class T, bool debug=false, bool progress_ind=false>
double benchmark (const V<T> n, const uint64_t repititions=1e6, const std::size_t buffer_size=1e6) {
	if (debug) {
		cout << "buffer size: " << sizeof(V<T>) * buffer_size / 1024. << "Kb" << endl;
		cout << "buffer num: " << repititions/buffer_size << endl;
	}

	V<T> array[n]; // create array of size n
	for (int i=0; i < n; i++) { // fill it
		array[i] = 2*i;
	}
	T benchmarkSuite(array, array+n); // copy it / initialize benchmark suite
	
	V<T>* rand_buffer = new V<T>[buffer_size]; // since this can be quite huge we better use heap memory

	//std::chrono::time_point<std::chrono::system_clock> start, end;
        time_point<std::chrono::system_clock> start, end; //you already defined the template above right?

	std::chrono::duration<double> elapsed_seconds;

	for (int i=0; i < repititions/buffer_size; i++) {
		// create a bunch of uniformly distributed random numbers
		random_number_buffer<V<T>>(rand_buffer, buffer_size, V<T>(0), 2*n);

		start = std::chrono::system_clock::now(); // tic

		for (int k=0; k<buffer_size; k++) { // insert & immediatly delete elements from the buffer to the container
			benchmarkSuite.benchmark(rand_buffer[k]);
		}

		end = std::chrono::system_clock::now(); // toc

		elapsed_seconds += end-start;

		if (progress_ind) {
			cout << '\r' << "progress: " << 100*((float) (i+1)*buffer_size)/repititions << "%";
			cout.flush();
		}
	}
	if (progress_ind)
		cout << '\r';

	delete[] rand_buffer; // free memory

	return elapsed_seconds.count();
}

#include <iomanip>

using std::setw;

int main () {
	#define debug
	
	cout << setw(12) << "std::vector"
		 << setw(12) << "std::list"
		 << setw(12) << "std::set" << endl;

	for (int i=10; i <= 1e10; i*=8) {
		cout << setw(12) << benchmark<Benchmark::Suite::StdSequenceContainer<unsigned int, std::vector>, true, true>(i)
			 << setw(12) << benchmark<Benchmark::Suite::StdSequenceContainer<unsigned int, std::list>>(i)
			 << setw(12) << benchmark<Benchmark::Suite::StdAssociativeContainer<unsigned int, std::set>>(i) << endl;
	}
}
