#ifndef EXPRESSION_HPP
#	define EXPRESSION_HPP

struct Composition;

template <class Op, class Enable = void>
struct ExpressionEvaluator {
	template <class T, class V>
	static V apply (const T expr, V val) {
		return Op::apply(expr.getLeft()(val), expr.getRight()(val));
	}
};

template <class Op>
struct ExpressionEvaluator<Op, typename std::enable_if<std::is_same<Composition, Op>::value>::type> {
	template <class T, class V>
	static V apply (const T expr, V val) {
		return Op::apply(expr.getLeft(), expr.getRight()(val));
	}
};

template <class L, class R, class Op>
class Expression {
protected:
	L left_;
	R right_;
public:
	typedef Expression type;
	typedef L left_type;
	typedef R right_type;
	typedef Op operator_type;

	Expression (const L l, const R r) : left_(l), right_(r) {}

	template <class T>
	T operator() (T x) const {
		return ExpressionEvaluator<Op>::apply(*this, x);
		/*if (std::is_same<Op, Composition>::value) {
			return Op::apply(left_(right_(x)));
		}

		return Op::apply(left_(x), right_(x));*/
	}

	const L getLeft () const { return left_; }

	const R getRight () const { return right_; }
};

/*template <class L, class R, class Op>
Expression<L, R, Op> expression() {
	return Expression<L, R, Op>(l, r);
}*/

#endif