namespace simpson {
	namespace traits {
		/*
		 * numerical helper
		 */
		template <class T, bool IsNumeric, bool IsInteger> struct numerical_helper {
			typedef T type;
		};

		// isNumeric && floatingPoint
		template <class T> struct numerical_helper<T, true, false> {
			typedef T type;
		};

		// isInteger
		template <class T> struct numerical_helper<T, true, true> {
			typedef double type;
		};

		/*
		 * domain_type trait
		 */
		// primary template
		template <class F> struct domain_type {
			template<class T> using argument_type = typename T::argument_type;

			typedef typename numerical_helper<argument_type<F>, 
												std::numeric_limits<argument_type<F>>::is_specialized, 
												std::numeric_limits<argument_type<F>>::is_integer>::type type;
		};

		// partial specialization for function pointers
		template <class R, class T> struct domain_type<R(T)> {
			typedef typename numerical_helper<T,
												std::numeric_limits<T>::is_specialized, 
												std::numeric_limits<T>::is_integer>::type type;
		};

		/*
		 * result_type trait
		 */
		// primary template
		template <class T> struct result_type {
			typedef typename T::result_type type;
		};

		// partial specialization for function pointers
		template <class R, class T> struct result_type<R(T)> {
			typedef typename numerical_helper<R,
										 std::numeric_limits<R>::is_specialized, 
										 std::numeric_limits<R>::is_integer>::type type;	
		};
	}
}