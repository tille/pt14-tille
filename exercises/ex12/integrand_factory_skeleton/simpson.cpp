#include <stdexcept>
#include "function.hpp"
#include "simpson.hpp"

Function::result_type integrate(const Function::argument_type a, const Function::argument_type b, const unsigned bins, const Function& f) {
    if (bins==0)
        throw std::logic_error("integrate(..) : number of bins has to be positive.");

    typedef Function::argument_type arg_type;
    typedef Function::result_type res_type;

    arg_type half_bin_size=(b-a)/static_cast<arg_type>(2*bins), x_;
    res_type sum_odd=f(a+half_bin_size), sum_even(0);

    for (unsigned i=1; i<bins; ++i) {
        x_=a+2*i*half_bin_size;
        sum_even+=f(x_);
        sum_odd+=f(x_+half_bin_size);
    }

    return (2.*sum_even + 4.*sum_odd + f(a) + f(b)) * half_bin_size / 3.;
}
