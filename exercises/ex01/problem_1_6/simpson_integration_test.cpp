#include <limits>
#include <assert.h>
#include "include/simpson_integration.h"

double f(double x) {
	return x*(1-x);
}

int main () {
	double result = simpson_integration(&f, 0, 1, 1);

	// Result should be 1/6
	assert((result - double(1)/6) < std::numeric_limits<double>::epsilon());

	return 0;
}