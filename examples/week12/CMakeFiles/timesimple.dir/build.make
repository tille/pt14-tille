# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/local/bin/cmake

# The command to remove a file.
RM = /opt/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/troyer/git/pt14/examples/week12

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/troyer/git/pt14/examples/week12

# Include any dependencies generated for this target.
include CMakeFiles/timesimple.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/timesimple.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/timesimple.dir/flags.make

CMakeFiles/timesimple.dir/timesimple.cpp.o: CMakeFiles/timesimple.dir/flags.make
CMakeFiles/timesimple.dir/timesimple.cpp.o: timesimple.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /Users/troyer/git/pt14/examples/week12/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/timesimple.dir/timesimple.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/timesimple.dir/timesimple.cpp.o -c /Users/troyer/git/pt14/examples/week12/timesimple.cpp

CMakeFiles/timesimple.dir/timesimple.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/timesimple.dir/timesimple.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /Users/troyer/git/pt14/examples/week12/timesimple.cpp > CMakeFiles/timesimple.dir/timesimple.cpp.i

CMakeFiles/timesimple.dir/timesimple.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/timesimple.dir/timesimple.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /Users/troyer/git/pt14/examples/week12/timesimple.cpp -o CMakeFiles/timesimple.dir/timesimple.cpp.s

CMakeFiles/timesimple.dir/timesimple.cpp.o.requires:
.PHONY : CMakeFiles/timesimple.dir/timesimple.cpp.o.requires

CMakeFiles/timesimple.dir/timesimple.cpp.o.provides: CMakeFiles/timesimple.dir/timesimple.cpp.o.requires
	$(MAKE) -f CMakeFiles/timesimple.dir/build.make CMakeFiles/timesimple.dir/timesimple.cpp.o.provides.build
.PHONY : CMakeFiles/timesimple.dir/timesimple.cpp.o.provides

CMakeFiles/timesimple.dir/timesimple.cpp.o.provides.build: CMakeFiles/timesimple.dir/timesimple.cpp.o

# Object files for target timesimple
timesimple_OBJECTS = \
"CMakeFiles/timesimple.dir/timesimple.cpp.o"

# External object files for target timesimple
timesimple_EXTERNAL_OBJECTS =

timesimple: CMakeFiles/timesimple.dir/timesimple.cpp.o
timesimple: CMakeFiles/timesimple.dir/build.make
timesimple: CMakeFiles/timesimple.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable timesimple"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/timesimple.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/timesimple.dir/build: timesimple
.PHONY : CMakeFiles/timesimple.dir/build

CMakeFiles/timesimple.dir/requires: CMakeFiles/timesimple.dir/timesimple.cpp.o.requires
.PHONY : CMakeFiles/timesimple.dir/requires

CMakeFiles/timesimple.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/timesimple.dir/cmake_clean.cmake
.PHONY : CMakeFiles/timesimple.dir/clean

CMakeFiles/timesimple.dir/depend:
	cd /Users/troyer/git/pt14/examples/week12 && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/troyer/git/pt14/examples/week12 /Users/troyer/git/pt14/examples/week12 /Users/troyer/git/pt14/examples/week12 /Users/troyer/git/pt14/examples/week12 /Users/troyer/git/pt14/examples/week12/CMakeFiles/timesimple.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/timesimple.dir/depend

