/** @file simpson.cpp 
    @brief Contains the definition of the simpson integration function.
    
*/

/** @fn Function::result_type integrate(const Function::argument_type a, const Function::argument_type b, const unsigned bins, const Function& f)
    @param [in] a Beginning (End) of integration interval.
    @param [in] b End (Beginning) of integration interval.
    @param [in] bins Number of bins to be used for the discretization of the integration interval.
    @param [in] f Object of class type derived from Function class used for the integration.

    @pre 

    - The domain of the function @p f shall contain the interval [min(@p a,@p b),max(@p a,@p b)].
    - The boundary values @p a and @p b shall be convertible to Function::argument_type type.
    - The parameter @p bins shall be convertible to unsigned int type.
    - The parameter @p bins shall be strictly non-negative ( bins > 0 ). 
 
    @post 
   
    - The value returned will approximate the integral of
      the function f(x) over the interval [min(a,b),max(a,b)]
      by the use of the Simpson rule with 'bins' equally sized
      intervals.
 
    @throw std::logic_error
    will be thrown in case the number of bins provided is not strictly positive. 
 
   The interval given by the boundaries 'a' and 'b' will be divided into 'bins' equally sized bins. The function f will be aproximated by a parabola using the function values at the bin-boundaries and bin-midpoint. the integral will be approximated by the sum of the integrals over each bin of the corresponding interpolating parabola. This corresponds to the simpson integration method explained in http://en.wikipedia.org/wiki/Simpson's_rule. The number of function calls is 2*bins+1. 
*/

#include <stdexcept>
#include "function.hpp"
#include "simpson.hpp"

Function::result_type integrate(const Function::argument_type a, const Function::argument_type b, const unsigned bins, const Function& f) {
    if (bins==0)
        throw std::logic_error("integrate(..) : number of bins has to be positive.");

    typedef Function::argument_type arg_type;
    typedef Function::result_type res_type;

    arg_type half_bin_size=(b-a)/static_cast<arg_type>(2*bins), x_;
    res_type sum_odd=f(a+half_bin_size), sum_even(0);

    for (unsigned i=1; i<bins; ++i) {
        x_=a+2*i*half_bin_size;
        sum_even+=f(x_);
        sum_odd+=f(x_+half_bin_size);
    }

    return (2.*sum_even + 4.*sum_odd + f(a) + f(b)) * half_bin_size / 3.;
}
