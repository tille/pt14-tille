#include "variable.hpp"
#include "constant.hpp"
#include "derivative.hpp"
#include "operators.hpp"
#include "stream_operators.hpp"

#include <cmath>

struct Cos;

struct Sin {
	Sin() {};

	typedef Cos derivative_type;

	template <class T>
	auto operator() (const T value) const -> decltype(std::sin(value)) {
		return std::sin(value);
	}

	Cos derivative() const ;
};

struct Cos {
	Cos() {};

	typedef Expression<Constant<int>, Sin, Multiply> derivative_type;

	template <class T> // todo: ensure T numeric_type
	auto operator() (const T value) const -> decltype(std::cos(value)) {
		return std::cos(value);
	}

	derivative_type derivative() const;
};

inline Cos::derivative_type Cos::derivative() const {
	const Sin sin;
	return constant(-1) * sin;
}

inline Cos Sin::derivative() const {
	const Cos cos;
	return cos;
}

// pre: T 
template <class T>
Expression<Sin, T, Composition> sin(T expr) {
	const Sin sin;
	return Expression<Sin, T, Composition>(sin, expr);
}

template <class T>
Expression<Cos, T, Composition> cos (T expr) {
	const Cos cos;
	return Expression<Cos, T, Composition>(cos, expr);
}

std::ostream& operator<<(std::ostream& stream, Cos const var) { 
  return stream << "cos";
}

std::ostream& operator<<(std::ostream& stream, Sin const var) { 
  return stream << "sin";
}

int main () {
	Variable<float> x;
	const Constant<float> c(2);
	auto func = sin(cos(x)*x)*x+x;
	auto derivative = Differentiator<decltype(func)>::derivative(func);
	std::cout << "f(x) = " << func << std::endl;
	std::cout << "f'(x) = " << derivative << std::endl;
	std::cout << "f(pi) = " << func(3.14) << std::endl;
	std::cout << "f'(pi) = " << derivative(3.14) << std::endl;
}