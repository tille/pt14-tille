#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

void expandArray(double* &array, int length) {
//void expandArray(double array[], const int length) {
	// allocate some new space
	double* newArray = new double[length*2];
	// copy the contents of the original array
	std::copy(array, array+length, newArray);
	// free memory
	delete[] array;
	// reasign 
	array = newArray;
}

int main () {
	int size = 0;
	int length = 10;
	double* array = new double[length];

	// read some input from the user
	string input;
	for (size = 0; getline(std::cin, input) && !input.empty(); size++) {
		// double the size of the array if all space is already used
		if (length<=size) {
                  //length *= 2; //increment here and use length
                  //instead of length*2 inside expandArray
			expandArray(array, length);
			length *= 2; //increment
		}

		array[size] = std::stoi(input);
	}

        //while loop with a counter (size) is more conventional

	// normalize
	//	calculate the sum
	double sum;
	std::for_each(array, array+size, [&sum](double value) {
		sum += value;
	});
	//	do the actual conversion
	std::for_each(array, array+size, [sum](double& value) {
		value = value/sum;
	});

	// print the result
	for (int j=size-1; j>=0; j--) {
		cout << array[j] << endl;
	}
}
