class thread : noncopyable
{
public:
  thread();
  ~thread();
  
  void swap(thread& x);
  // …move support… 
  
  typedef platform-specific-type native_handle_type;
  native_handle_type native_handle();
  
  // launch
  template <class F>
  explicit thread(F f);
  
  template <class F,class A1,class A2,...>
  thread(F f,A1 a1,A2 a2,...);	
  