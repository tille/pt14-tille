cmake_minimum_required(VERSION 2.8)

project(SimpsonIntegration)

# add simpson_integration library
add_library(simpson_integration_library STATIC include/simpson_integration.cpp)

# add tclap header library for cmd arg parsing
include_directories( BEFORE tclap-1.2.1/include )

# include the actuall library
include_directories( include )

# Add executable
add_executable( simpson_integration simpson_integration.cpp )
target_link_libraries(simpson_integration simpson_integration_library)

# Add test with a polynomial function of degree two that should give exact results
enable_testing()

add_executable( simpson_integration_test simpson_integration_test.cpp )
target_link_libraries(simpson_integration_test simpson_integration_library)

add_test ( polynomial_of_degree_two simpson_integration_test )