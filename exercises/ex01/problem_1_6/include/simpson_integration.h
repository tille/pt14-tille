//#ifndef SIMPSON_INTEGRATION_HPP - include guards!!
//#define SIMPSON_INTEGRATION_HPP

/**
 * Simpson integration
 * @var f pointer to the function to integrate
 * @var a lower bound
 * @var b upper bound
 * @var N number of steps 
 */
double simpson_integration (double (*f)(double), double a, double b, int N);
//double simpson_integration (double (*)(double), const double, const double, const int); 
//- use const and argument names are not needed in declaration

//#endif
