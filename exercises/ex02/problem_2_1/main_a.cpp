#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int main () {
	int size = 0;
	int length = 10;
        //const int length = 10; - use const. variable length static
        //arrays should not be used in c++
	double array[length];

	// Read some input from the user
	string input;
	for (size = 0; size<10 && getline(std::cin, input) && !input.empty(); size++) {
		array[size] = std::stoi(input);
	}

        //stream is nicer: std::cin >> array[size] 

	// normalize
	//	calculate the sum
	double sum;
	std::for_each(array, array+size, [&sum](double value) {
		sum += value;
	});

        //const double sum(std::accumulate(array,array+size,0.0));

	//	do the actual conversion
	std::for_each(array, array+size, [sum](double& value) {
		value = value/sum;
	});

	// print the result
	for (int j=size-1; j>=0; j--) {
		cout << array[j] << endl;
	}
}
