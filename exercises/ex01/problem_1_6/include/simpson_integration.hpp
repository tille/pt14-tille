#ifndef SIMPSON_INTEGRATION_HPP
#define SIMPSON_INTEGRATION_HPP

/**
 * Simpson integration
 * @var f pointer to the function to integrate
 * @var a lower bound
 * @var b upper bound
 * @var N number of steps 
 */
template <typename F>
double simpson_integration (F f, const double a, const double b, const int N);

class MathFunction {
public:
	double operator() (double x);
};

#endif
